#
# Amdatu Blueprint base feature
#
blueprint-feature.base: base
blueprint-deps.base: \
	org.apache.felix:org.apache.felix.configadmin:1.9.16,\
	org.apache.felix:org.apache.felix.dependencymanager.annotation:5.0.1,\
	org.apache.felix:org.apache.felix.dependencymanager.runtime:4.0.7,\
	org.apache.felix:org.apache.felix.dependencymanager:4.6.0,\
	org.apache.felix:org.apache.felix.eventadmin:1.5.0,\
	org.apache.felix:org.apache.felix.framework:6.0.3,\
	org.apache.felix:org.apache.felix.metatype:1.2.2,\
	org.ops4j.pax.logging:pax-logging-api:2.0.2,\
	org.ops4j.pax.logging:pax-logging-log4j2:2.0.2,\
	org.osgi:org.osgi.core:6.0.0,\
	org.osgi:org.osgi.service.cm:1.6.0,\
	org.osgi:org.osgi.service.event:1.3.1,\
	org.osgi:org.osgi.service.log:1.4.0,\
	org.osgi:org.osgi.service.metatype.annotations:1.3.0,\
	org.osgi:org.osgi.service.metatype:1.3.0,\
	org.osgi:osgi.annotation:6.0.1,\
	org.slf4j:slf4j-api:1.7.25

blueprint-deps.test: \
    biz.aQute.bnd:biz.aQute.launchpad:4.3.1,\
	net.bytebuddy:byte-buddy-agent:1.9.3,\
	net.bytebuddy:byte-buddy:1.9.3,\
	org.apache.servicemix.bundles:org.apache.servicemix.bundles.junit:4.12_1,\
	org.junit.jupiter:junit-jupiter-api:${junit-version},\
    org.junit.jupiter:junit-jupiter-params:${junit-version},\
    org.junit.platform:junit-platform-commons:1.4.1,\
    org.junit.platform:junit-platform-launcher:1.4.1,\
    org.junit.vintage:junit-vintage-engine:${junit-version},\
    org.mockito:mockito-core:2.23.4,\
    org.objenesis:objenesis:2.6

#
# Build
#
-buildpath.blueprint-base: \
	${if;(buildfeaturesMerged[]=base); \
		org.apache.felix.dependencymanager,\
		org.apache.felix.dependencymanager.annotation,\
		org.osgi.service.cm,\
		org.osgi.service.event,\
		org.osgi.service.metatype,\
		org.osgi.service.metatype.annotations,\
		org.osgi.service.log,\
		osgi.annotation,\
		osgi.core,\
		slf4j.api\
	}

-testpath.blueprint-base: \
    ${if;(buildfeaturesMerged[]=base); \
        biz.aQute.launchpad,\
        org.apache.servicemix.bundles.junit,\
        org.junit.platform:junit-platform-launcher,\
        org.junit.platform:junit-platform-commons,\
        org.junit.jupiter:junit-jupiter-api,\
        org.junit.jupiter:junit-jupiter-params,\
        org.junit.vintage:junit-vintage-engine,\
        org.mockito.mockito-core,\
        org.objenesis,\
        net.bytebuddy.byte-buddy,\
        net.bytebuddy.byte-buddy-agent\
    }

#
# Run
#
-runbundles.blueprint-base: \
	${if;(runfeaturesMerged[]=base); \
		org.apache.felix.dependencymanager.runtime,\
		org.apache.felix.dependencymanager,\
		org.apache.felix.configadmin,\
		org.apache.felix.eventadmin,\
		org.apache.felix.metatype,\
		org.ops4j.pax.logging.pax-logging-api,\
		org.ops4j.pax.logging.pax-logging-log4j2\
	}
